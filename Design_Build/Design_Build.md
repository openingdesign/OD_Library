XYZ

* It's easy to understand that front loading all the disciplines early in the project would create a better project.


* Owner
	* Reasons from owner
		* generally take less time, of better quality, and less cost
		* Single point of contact
		* Construction costs or projections are typically more accurate
		* no confrontation btwn architect and contractor
			* minimize claims and damages
			* eliminates finger pointing
			* minimizes change orders
	* Concerns
		* How does an architect protect their interests?  who's their advocate?
			* Protect our representation, just like a sole architect would have to do.
			* Have contractor's early input on the design and constructability actually improves quality
			* Open book
				* Share numbers publicly
			* All communication with contractors/subs is transparent


* From Architect's perspective
	* better control over the quality of design
	* quicker
	* Connections construction knowledge to design
		* I can lean on the subs to help with improving the design from a cost/time/quality perspective
	* Just in time detailing
		* I don't have to design and detail out everything in the beginning
			* Can improve just in-time detailing, informed by immediate research and sub input
	* More profitable

- Reasons to Contractor
	- Sharing risks and rewards
	- Architect is on your team... no more finger pointing
		- It's not a design problem, or construction problem, it's OUR problem
	- Project specific
		- Have a log of lessons learned can impart to subs
		- estimates/cost from previous project
	- Their head is in the project, being there from the beginning


* GMP
	* if get under that, share profit
	* 

* Types of contracts
	* Joint venture: Create separate design-build entity
	* 
* Costs
	* first cost vs. life-cycle cost
	* 

* Insurance
	* General liability insurance?
	* Builder's Risk
	* Worker's compensation
	* Bonding?
		* contractor's realm

* All subs in
	* open to the public
	* 

* Why this project.
	* We can reuse a lot of the drawings, if the design stays somewhat the same
		* well the architectural and structural
	* Applying lessons learned from previous project

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTQ1NjU1NzAzNywyMDU1OTk5NTIyLC00Mz
U0MjI4MzAsMTQ4NTE2MDUyNCw3NDI0OTkyNjIsMTQ2ODU4MzQx
MCwxODU0NTg1OTMyLC0yMzg2MzU1NTksLTk0MjY0NTUwMSwyMD
M1MTYxNjI1LDE1MTczMTMzMTMsMTIwNDk1MzE0MSwxMDU2NTEy
NDA4LC0zNzY0Njg0OTMsMTM4MDAxMzUzOSw0NzkzMzkyMDEsMj
A0ODkxMjk3NiwtNDA4NzE0MzU2LC0xNDExMTQxODk5XX0=
-->