- Use of New? of existing?
	- what was the previous use
- Site
	- easements?




- Landlord?
  
  Title search
  - any encumbrances on the title?
- SF over overall building
  - SF of scope
- **Neighboring tenants**
- **Sprinklers**
- **Adequate Exiting**
- **ADA compliance**
	- **An accessible entrance**
	- **An accessible route to the primary function area**
	- **At least one accessible restroom for each gender**
	- **Accessible Drinking Fountains**
	- **Parking**
	- **Storage and Alarms**
- **Salvage**
  - Acoustical ceilings
  - Doors
  - Lighting
  - Finishes
- Structure
  - Change of occupancy? Change of loads?  Existing structure to accommodate?
  - Determine structure to determine construction type
  - structural composition of walls/floor/ceilings
	  - thickness
- Fire separation
  - Check doors
  - Stairs
- HVAC
  - locations of main trunks and equipment
  - suggested locations of proposed equipment
- Electrical
  - Lighting
- Plumbing
  - existing restrooms enough for new use?
  - sewer stub location
  - water stub location
  - location of hot water heater
  - existing drinking fountain
- Sprinklers
- Sitting
  - Parking
  - Signage
    - zoning implications
- Finishes - general strategy
- Allocated budget
- Schedule
- Drawings
  - Permit set
  - Reflected ceiling
  - Finish plan
  - MEP?
- Existing CAD files
- What are the neighboring tenants
  - possibility measure or do reconnaissance on their suite to find any conflicts with what's planned 


### Restaurants


- Main spaces
  
  - Entry
  
  - Kitchen
    
    - Prep
      - dedicated food prep sink
      - handsink
    - Cooking
      - Cooking: Grease
        - fryers
        - griddle
        - Type I hoods
        - 
        - Grease traps
      - Cooking: nonGrease
        - ovens
        - toaster
        - Type II hood
      - handsink
      - hoods
  
  - Dishwashing
    
    - Dish washing
      - dishwasher or 3 compartment sink
    - Type II Hood if heating
      - no hood, for smaller chemical DW
    - hand sink
  
  - storage
    
    - dry
    - refrigeration/freezer
    - mop sink, required
    - garbage strategy
  
  - Seating
    
    - seating types
  
  - Register
    
    - check out
    - displays
    - sneeze guards
    - refrigerated displays
    - Signage
    - Condiment area
  
  - Bathrooms
    
    - dishwasher or 3 compartment sink
    - taps
      - refrigeration lines
  
  - Restrooms
    
    - finishes
    - baby changing
    - Employee use
      - If public, faucet non-hand operated
  
  - offices
  
  - Equipment specs


- What's the function?
	- space and function types?
	- one floor?
	- MEP requirements?
		- HVAC
			- heating/cooling approach
			- ventilation?
	- Help with finishes?

- Site Layout
	- Zoning 
		- entitlements?
	- Parking
	- Zoning
	- Landscaping
- Phases
- Schedule
- Equipment layout?
- 

- 


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5OTg3ODQ3MjAsMTgyMDIxNTk3LDM3Nj
M2MDI1NSwxOTM5MTAyMjUzLC04MDIzNDA4MTcsLTM1MDc5OTU0
NSwyMDY5OTI4MTIyLC0xNTgyNjc5MTY4LDYyMzUzOTY3NiwtMT
U0MTY5ODY5MCwtMjEyMTkyNzMyOSwtMTA1MDQ1NTQxMywtMjcz
NzYxNjcsNjUzMDIwODI0LC00ODEzMjk4NDEsOTAzMDk2NzU5LD
EzMTMxNjE3MDAsMTg1Njk5NDI1MCw2NDA1MTEwNTcsLTE2NTkw
MDQ1OThdfQ==
-->