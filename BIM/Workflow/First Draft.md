**First Draft** is a web-based, real-time whiteboarding tool--allowing users to sketch on plans/drawings in real time with one another.

- With this tool, it's best to use a computer with a mouse, as it's easier to sketch with.

- You can also use the mouse wheel to zoom in/out, just like google maps.  

- The pencil tool![image.png](https://gitlab.com/openingdesign/OD_Library/-/raw/master/BIM/Workflow/imgs/fd_pencil.png) will allow you to sketch on the plan. 

- Use the text tool ![image.png](https://gitlab.com/openingdesign/OD_Library/-/raw/master/BIM/Workflow/imgs/fd_text.png)to add text.

- The ![image.png](https://gitlab.com/openingdesign/OD_Library/-/raw/master/BIM/Workflow/imgs/fd_hand.png) tool will allow you to pan around. You can also hold down your middle mouse button and drag.  

- Or use the export tool ![image.png](https://gitlab.com/openingdesign/OD_Library/-/raw/master/BIM/Workflow/imgs/fd_download.png) in order to download the image locally.

All the sketches and markups will be shared with everyone in real time.  



---



How to Start a new instance

- Go https://openingdesign.first-draft.xyz and it will automatically create a unique URL that you can share with your collaborator or...

- You can type in a unique url.  Start with `https://openingdesign.first-draft.xyz/d/`, after which add a unique name. 
  
  - Example: https://openingdesign.first-draft.xyz/d/addyouruniquenamehere
